<?php

/* 
 * The MIT License
 *
 * Copyright 2016 Maxim Eltratov <Maxim.Eltratov@yandex.ru>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

//Filename: /module/Blog/src/Blog/Factory/ZendDbSqlMapperFactory.php
namespace Blog\Factory\Mapper;

use Blog\Mapper\ZendDbSqlMapper;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Hydrator\ClassMethods;
use Blog\Hydrator\Tag\TagHydrator;
use Zend\Config\Config;

class ZendDbSqlMapperFactory implements FactoryInterface
{
    /**
     * Create post mapper
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $aggregatehydrator = $serviceLocator->get('Zend\Hydrator\AggregateHydrator');

        $classMethodsHydrator = new ClassMethods(false);
        $tagHydrator = new TagHydrator();
        
        $category = $serviceLocator->get('Blog\Model\CategoryInterface');
        $tag = $serviceLocator->get('Blog\Model\TagInterface');
        $post = $serviceLocator->get('Blog\Model\PostInterface');
        
        $config = new Config($serviceLocator->get('config'));
        
        return new ZendDbSqlMapper(
            $serviceLocator->get('Zend\Db\Adapter\Adapter'),
            $aggregatehydrator,
            $tagHydrator,
            $classMethodsHydrator,
            $post,
            $category,
            $tag,
            $config->blog_module
        );
    }
}