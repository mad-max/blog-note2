<?php

/* 
 * The MIT License
 *
 * Copyright 2016 Maxim Eltratov <Maxim.Eltratov@yandex.ru>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Blog\Controller;

use Blog\Service\PostServiceInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Paginator\Paginator;
use Zend\Config\Config;
use Blog\Exception\RecordNotFoundBlogException;
use Zend\Validator\Date;
use Blog\Service\DateTimeInterface;

class ListController extends AbstractActionController
{
    /**
     * @var \Blog\Service\PostServiceInterface
     */
    protected $postService;
    
    protected $dateValidator;
    
    protected $datetime;


    /**
     * @var Zend\Config\Config
     */
    protected $config;
    
    public function __construct(
        PostServiceInterface $postService, 
        Date $dateValidator, 
        DateTimeInterface $datetime, 
        Config $config
    ) {
        $this->postService = $postService;
        $this->dateValidator = $dateValidator;
        $this->datetime = $datetime;
        $this->config = $config;
    }

    public function indexAction()
    {
        $paginator = $this->postService->findAllPosts();
        $this->configurePaginator($paginator);
        
        return new ViewModel([
            'posts' => $paginator,
            'route' => 'blog/page'
        ]);
    }
    
    public function listPostsAction()
    {
        $paginator = $this->postService->findAllPosts();
        $this->configurePaginator($paginator);
        
        return new ViewModel([
            'posts' => $paginator,
            'route' => 'blog/listPosts'
        ]);
    }
    
    public function listPostsByCategoryAction()
    {
        $categoryId = $this->params()->fromRoute('id');
        
        try {
            $category = $this->postService->findCategoryById($categoryId);
        } catch (\InvalidArgumentException $ex) {
            return $this->notFoundAction();
        }
        
        $paginator = $this->postService->findPostsByCategory($category);
        $this->configurePaginator($paginator);
        
        $model = new ViewModel([
            'posts' => $paginator,
            'route' => 'blog/listPostsByCategory'
        ]);
        $model->setTemplate('blog/list/list-posts');
        
        return $model;
    }
    
    public function listPostsByTagAction()
    {
        $tagId = $this->params()->fromRoute('id');

        try {
            $tag = $this->postService->findTagById($tagId);
        } catch (\InvalidArgumentException $ex) {
            return $this->notFoundAction();
        }
        
        $paginator = $this->postService->findPostsByTag($tag);
        $this->configurePaginator($paginator);
        
        $model = new ViewModel(array(
            'posts' => $paginator,
            'route' => 'blog/listPostsByTag'
        ));
        $model->setTemplate('blog/list/list-posts');
        
        return $model;
    }
    
    public function listPostsByPublishedAction()
    {
        $since = $this->params()->fromRoute('since');
        $to = $this->params()->fromRoute('to');
        
        $since . ' 00:00:00';
        $to . ' 23:59:59';
        
        $dateTimeFormat = $this->config->dateTime->dateTimeFormat;
        $this->dateValidator->setFormat($dateTimeFormat);
                
        if ($this->dateValidator->isValid($since)) {
            $since = $this->datetime->createFromFormat(dateTimeFormat, $since);
        }
        
        if ($this->dateValidator->isValid($to)) {
            $to = $this->datetime->createFromFormat(dateTimeFormat, $to);
        }

        $paginator = $this->postService->findPostsByPublishDate($since, $to);
        $this->configurePaginator($paginator);
        
        $model = new ViewModel(array(
            'posts' => $paginator,
            'route' => 'blog/listPostsByPublished'
        ));
        $model->setTemplate('blog/list/list-posts');
        
        return $model;
    }
    
    public function detailPostAction()
    {
        $id = $this->params()->fromRoute('id');
        try {
            $post = $this->postService->findPostById($id);
        } catch (RecordNotFoundBlogException $ex) {
            return $this->notFoundAction();
        }

        return new ViewModel(array(
            'post' => $post
        ));
    }
    
    public function listCategoriesAction()
    {
        $paginator = $this->postService->findAllCategories();
        $this->configurePaginator($paginator);
        
        return new ViewModel(['categories' => $paginator]);
    }
    
    public function detailCategoryAction()
    {
        $id = $this->params()->fromRoute('id');
        try {
            $category = $this->postService->findCategoryById($id);
        } catch (\InvalidArgumentException $ex) {
            return $this->notFoundAction();
        }

        return new ViewModel(array(
            'category' => $category
        ));
    }
    
    public function listTagsAction()
    {
        $paginator = $this->postService->findAllTags();
        $this->configurePaginator($paginator);
        
        return new ViewModel(['tags' => $paginator]);
    }
    
    public function detailTagAction()
    {
        $id = $this->params()->fromRoute('id');
        try {
            $tag = $this->postService->findTagById($id);
        } catch (\InvalidArgumentException $ex) {
            return $this->notFoundAction();
        }

        return new ViewModel(array(
            'tag' => $tag
        ));
    }
    
    private function configurePaginator(Paginator $paginator) 
    {
        $page = (int) $this->params()->fromRoute('page');
        $page = ($page < 1) ? 1 : $page;
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($this->config->listController->ItemCountPerPage);
        
        return $this;
    }
}
