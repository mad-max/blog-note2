<?php

/* 
 * The MIT License
 *
 * Copyright 2016 Maxim Eltratov <Maxim.Eltratov@yandex.ru>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace BlogTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Blog\Model\Post;

class WriteControllerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    public function setUp()
    {
        $this->setApplicationConfig(
            include '/../../../config/application.config.php'
        );
        parent::setUp();
    }
    
    public function testAddActionCanBeAccessed()
    {   
        $this->dispatch('/blog/add');
        $this->assertModuleName('Blog');
        $this->assertControllerName('Blog\Controller\Write');	//Controller name was defined in the module.config.php
        $this->assertControllerClass('WriteController');
        $this->assertMatchedRouteName('blog/add');
        $this->assertResponseStatusCode(200);
    }
    
    public function testAddActionRedirectsAfterValidPost()
    {
        $postObject = new Post();
        
        $postServiceMock = $this->getMockBuilder('Blog\Service\PostServiceInterface')
            ->disableOriginalConstructor()->getMock();
        $postServiceMock->expects($this->once())->method('savePost')
            ->will($this->returnValue($postObject));
        
        $postFormMock = $this->getMockBuilder('Blog\Form\PostForm')
            ->disableOriginalConstructor()->getMock();
        $postFormMock->expects($this->once())->method('isValid')
            ->will($this->returnValue(true));
        $postFormMock->expects($this->once())->method('getData')
            ->will($this->returnValue($postObject));

        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Blog\Service\PostServiceInterface', $postServiceMock);
        $serviceManager->setService('Blog\Form\PostForm', $postFormMock);
        
        $postData = array(
            'id'  => '',
            'title' => 'My title',
            'text' => 'My text'
        );
        $this->dispatch('/blog/add', 'POST', $postData);
        $this->assertModuleName('Blog');
        $this->assertControllerName('Blog\Controller\Write');	//Controller name was defined in the module.config.php
        $this->assertControllerClass('WriteController');
        $this->assertMatchedRouteName('blog/add');
        $this->assertResponseStatusCode(302);
        $this->assertRedirectTo('/blog');
    }
    
//    public function testAddActionAfterInvalidPost()
//    {
//        $postObject = new Post();
//        
//        $viewModel = new ViewModel(array());
//        
//        $postServiceMock = $this->getMockBuilder('Blog\Service\PostServiceInterface')
//            ->disableOriginalConstructor()->getMock();
//        $postServiceMock->expects($this->once())->method('savePost')
//            ->will($this->returnValue($postObject));
//        
//        $postFormMock = $this->getMockBuilder('Blog\Form\PostForm')
//            ->disableOriginalConstructor()->getMock();
//        $postFormMock->expects($this->any())->method('isValid')
//            ->will($this->returnValue(false));
//        $postFormMock->expects($this->once())->method('getData')
//            ->will($this->returnValue($postObject));
//
//        $serviceManager = $this->getApplicationServiceLocator();
//        $serviceManager->setAllowOverride(true);
//        $serviceManager->setService('Blog\Service\PostServiceInterface', $postServiceMock);
//        $serviceManager->setService('Blog\Form\PostForm', $postFormMock);
//        
//        $postData = array(
//            'id'  => '',
//            'title' => 'My title',
//            'text' => 'My text'
//        );
//        $this->dispatch('/blog/add', 'POST', $postData);
//        
//        $this->assertModuleName('Blog');
//        $this->assertControllerName('Blog\Controller\Write');	//Controller name was defined in the module.config.php
//        $this->assertControllerClass('WriteController');
//        $this->assertMatchedRouteName('blog/add');
//        $this->assertResponseStatusCode(200);
//    }
    
    public function testAddActionAfterFailedSavePost()
    {   
        $postObject = new Post();
        
        $postServiceMock = $this->getMockBuilder('Blog\Service\PostServiceInterface')
            ->disableOriginalConstructor()->getMock();
        $postServiceMock->expects($this->any())->method('savePost')
            ->will($this->throwException(new \Exception()));
        
        $postFormMock = $this->getMockBuilder('Blog\Form\PostForm')
            ->disableOriginalConstructor()->getMock();
        $postFormMock->expects($this->any())->method('isValid')
            ->will($this->returnValue(true));
        $postFormMock->expects($this->once())->method('getData')
            ->will($this->returnValue($postObject));

        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Blog\Service\PostServiceInterface', $postServiceMock);
        $serviceManager->setService('Blog\Form\PostForm', $postFormMock);
        
        $postData = array();
        $this->dispatch('/blog/add', 'POST', $postData);
        $this->assertResponseStatusCode(404);
        $this->assertModuleName('Blog');
        $this->assertControllerName('Blog\Controller\Write');	//Controller name was defined in the module.config.php
        $this->assertControllerClass('WriteController');
        $this->assertMatchedRouteName('blog/add');
    }
}