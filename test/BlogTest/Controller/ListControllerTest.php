<?php

/* 
 * The MIT License
 *
 * Copyright 2016 Maxim Eltratov <Maxim.Eltratov@yandex.ru>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace BlogTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Blog\Model\Post;

class ListControllerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    public function setUp()
    {
        $this->setApplicationConfig(
            include '/../../../config/application.config.php'
        );
        parent::setUp();
    }
	
    public function testIndexActionCanBeAccessed()
    {
        $postServiceMock = $this->getMockBuilder('Blog\Service\PostServiceInterface')
            ->disableOriginalConstructor()->getMock();

        $postServiceMock->expects($this->once())->method('findAllPosts')
            ->will($this->returnValue(array()));    //The mock is created so that it will return just an empty array

        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);    //By default, the Service Manager does not allow us to replace existing services.
        $serviceManager->setService('Blog\Service\PostServiceInterface', $postServiceMock); //Replacing the real instance with a mock.

        $this->dispatch('/blog');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Blog');
        $this->assertControllerName('Blog\Controller\List');	//Controller name was defined in the module.config.php
        $this->assertControllerClass('ListController');
        $this->assertMatchedRouteName('blog');
    }

    public function testDetailActionCanBeAccessed()
    {
        $postObject = new Post();
        
        $postServiceMock = $this->getMockBuilder('Blog\Service\PostServiceInterface')
            ->disableOriginalConstructor()->getMock();

        $postServiceMock->expects($this->once())->method('findPost')
            ->will($this->returnValue($postObject));
        
        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Blog\Service\PostServiceInterface', $postServiceMock);

        $this->dispatch('/blog/1');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Blog');
        $this->assertControllerName('Blog\Controller\List');	//Controller name was defined in the module.config.php
        $this->assertControllerClass('ListController');
        $this->assertMatchedRouteName('blog/detail');
    }
    
    public function testDetailActionRedirectsAfterFailedFindPost()
    {   
        $postServiceMock = $this->getMockBuilder('Blog\Service\PostServiceInterface')
            ->disableOriginalConstructor()->getMock();

        $postServiceMock->expects($this->once())->method('findPost')
            ->will($this->throwException(new \InvalidArgumentException()));

        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Blog\Service\PostServiceInterface', $postServiceMock);
        
        $this->dispatch('/blog/1');
        $this->assertResponseStatusCode(404);
        $this->assertModuleName('Blog');
        $this->assertControllerName('Blog\Controller\List');	//Controller name was defined in the module.config.php
        $this->assertControllerClass('ListController');
        $this->assertMatchedRouteName('blog/detail');
    }
}