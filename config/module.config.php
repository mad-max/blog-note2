<?php
return array(
    'service_manager' => array(
        'factories' => array(
            'Blog\Mapper\MapperInterface' => 'Blog\Factory\Mapper\ZendDbSqlMapperFactory',
            'Blog\Service\PostServiceInterface' => 'Blog\Factory\Service\PostServiceFactory',
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
            'Blog\Model\PostInterface' => 'Blog\Factory\Model\PostFactory',
            'Blog\Model\CategoryInterface' => 'Blog\Factory\Model\CategoryFactory',
            'Blog\Model\TagInterface' => 'Blog\Factory\Model\TagFactory',
            'Blog\Service\DateTimeInterface' => 'Blog\Factory\Service\DateTimeFactory',
            'Zend\Validator\Date' => 'Blog\Factory\Validator\DateValidatorFactory',
            'Blog\Hydrator\TagsHydrator' => 'Blog\Factory\Hydrator\TagsHydratorFactory',
            'Blog\Hydrator\CategoryHydrator' => 'Blog\Factory\Hydrator\CategoryHydratorFactory',
            'Blog\Hydrator\PostHydrator' => 'Blog\Factory\Hydrator\PostHydratorFactory',
            'Blog\Hydrator\DatesHydrator' => 'Blog\Factory\Hydrator\DatesHydratorFactory',
            'Zend\Hydrator\AggregateHydrator' => 'Blog\Factory\Hydrator\AggregateHydratorFactory',
            'Blog\Validator\IsPublishedRecordExistsValidatorInterface' => 'Blog\Factory\Validator\IsPublishedRecordExistsValidatorFactory'
        )
    ),
    'controllers' => array(
        'factories' => array(
            'Blog\Controller\List' => 'Blog\Factory\Controller\ListControllerFactory',
            'Blog\Controller\Write' => 'Blog\Factory\Controller\WriteControllerFactory',
            'Blog\Controller\Delete' => 'Blog\Factory\Controller\DeleteControllerFactory'
        ),
    ),
    'form_elements' => array(
        'factories' => array(
            'Blog\Form\PostForm' => 'Blog\Factory\Form\PostFormFactory',
            'Blog\Form\PostFieldset' => 'Blog\Factory\Form\PostFieldsetFactory',
            'Blog\Form\CategoriesFieldset' => 'Blog\Factory\Form\CategoriesFieldsetFactory',
            'Blog\Form\TagFieldset' => 'Blog\Factory\Form\TagFieldsetFactory',
            'Blog\Form\CategoryForm' => 'Blog\Factory\Form\CategoryFormFactory',
            'Blog\Form\CategoryFieldset' => 'Blog\Factory\Form\CategoryFieldsetFactory',
            'Blog\Form\TagForm' => 'Blog\Factory\Form\TagFormFactory',
        )
    ),
    'view_helpers' => [
        'factories' => [
            'FormatDateI18n' => 'Blog\Factory\View\Helper\FormatDateI18nFactory',
            'ArchiveDates' => 'Blog\Factory\View\Helper\ArchiveDatesFactory',
        ],
    ],
    
    'router' => array(
        'routes' => array(
            'blog' => array(
                'type'    => 'Literal',
                'options' => array(
                    // Change this to something specific to your module
                    'route'    => '/blog',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        //'__NAMESPACE__' => 'Blog\Controller',
                        'controller'    => 'Blog\Controller\List',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    // This route is a sane default when developing a module;
                    // as you solidify the routes for your module, however,
                    // you may want to remove it and replace it with more
                    // specific routes.
                    'page' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/page/:page',
                            'constraints' => array(
                                'page' => '[1-9]\d*',
                            ),
                            'defaults' => array(
                                'controller'    => 'Blog\Controller\List',
                                'action' => 'index'
                            ),
                        ),
                    ),
                    'detailPost' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/detail/post/:id',
                            'constraints' => array(
                                'id' => '[1-9]\d*',
                            ),
                            'defaults' => array(
                                'controller'    => 'Blog\Controller\List',
                                'action' => 'detailPost'
                            ),
                        ),
                    ),
                    'detailCategory' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/detail/category/:id',
                            'constraints' => array(
                                'id' => '[1-9]\d*',
                            ),
                            'defaults' => array(
                                'controller'    => 'Blog\Controller\List',
                                'action' => 'detailCategory'
                            ),
                        ),
                    ),
                    'detailTag' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/detail/tag/:id',
                            'constraints' => array(
                                'id' => '[1-9]\d*',
                            ),
                            'defaults' => array(
                                'controller'    => 'Blog\Controller\List',
                                'action' => 'detailTag'
                            ),
                        ),
                    ),
                    'listPosts' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/list/posts[/:page]',
                            'constraints' => array(
                                'page' => '[1-9]\d*',
                            ),
                            'defaults' => array(
                                'controller'    => 'Blog\Controller\List',
                                'action' => 'listPosts'
                            ),
                        ),
                    ),
                    'listPostsByCategory' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/list/posts/category[/:id[/:page]]',
                            'constraints' => array(
                                'id' => '[1-9]\d*',
                                'page' => '[1-9]\d*',
                            ),
                            'defaults' => array(
                                'controller'    => 'Blog\Controller\List',
                                'action' => 'listPostsByCategory'
                            ),
                        ),
                    ),
                    'listPostsByTag' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/list/posts/tag[/:id[/:page]]',
                            'constraints' => array(
                                'id' => '[1-9]\d*',
                                'page' => '[1-9]\d*',
                            ),
                            'defaults' => array(
                                'controller'    => 'Blog\Controller\List',
                                'action' => 'listPostsByTag'
                            ),
                        ),
                    ),
                    'listPostsByPublished' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/list/posts/published[/:page[/:since[/:to]]]',
                            'constraints' => array(
                                'page' => '[1-9]\d*',
                                'since' => '[a-zA-Z0-9_-]*',
                                'to' => '[a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller'    => 'Blog\Controller\List',
                                'action' => 'listPostsByPublished'
                            ),
                        ),
                    ),
                    'listArchivesPosts' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/list/archives/posts[/:year[/:month[/:page]]]',
                            'constraints' => array(
                                'page' => '[1-9]\d*',
                                'year' => '[1-9]\d*',
                                'month' => '[1-9]\d*',
                            ),
                            'defaults' => array(
                                'controller'    => 'Blog\Controller\List',
                                'action' => 'listArchivesPosts'
                            ),
                        ),
                    ),
                    'listCategories' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/list/categories[/:page]',
                            'constraints' => array(
                                'page' => '[1-9]\d*',
                            ),
                            'defaults' => array(
                                'controller'    => 'Blog\Controller\List',
                                'action' => 'listCategories'
                            ),
                        ),
                    ),
                    'listTags' => array(    //это название роута используется в контроллерах представлениях
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/list/tags[/:page]', //это будет показываться в адресной строке
                            'constraints' => array(
                                'page' => '[1-9]\d*',
                            ),
                            'defaults' => array(
                                'controller'    => 'Blog\Controller\List',
                                'action' => 'listTags'
                            ),
                        ),
                    ),
                    'addPost' => array(
                        'type'    => 'literal',
                        'options' => array(
                            'route'    => '/add/post',
                            'defaults' => array(
                                'controller' => 'Blog\Controller\Write',
                                'action' => 'addPost'
                            ),
                        ),
                    ),
                    'addCategory' => array(
                        'type'    => 'literal',
                        'options' => array(
                            'route'    => '/add/category',
                            'defaults' => array(
                                'controller' => 'Blog\Controller\Write',
                                'action' => 'addCategory'
                            ),
                        ),
                    ),
                    'addTag' => array(
                        'type'    => 'literal',
                        'options' => array(
                            'route'    => '/add/tag',
                            'defaults' => array(
                                'controller' => 'Blog\Controller\Write',
                                'action' => 'addTag'
                            ),
                        ),
                    ),
                    'editPost' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/edit/post/:id',
                            'constraints' => array(
                                'id' => '[1-9]\d*',
                            ),
                            'defaults' => array(
				'controller' => 'Blog\Controller\Write',
				'action' => 'editPost'
                            ),
                        ),
                    ),
                    'editCategory' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/edit/category/:id',
                            'constraints' => array(
                                'id' => '[1-9]\d*',
                            ),
                            'defaults' => array(
                                'controller' => 'Blog\Controller\Write',
                                'action' => 'editCategory'
                            ),
                        ),
                    ),
                    'editTag' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/edit/tag/:id',
                            'constraints' => array(
                                'id' => '[1-9]\d*',
                            ),
                            'defaults' => array(
                                'controller' => 'Blog\Controller\Write',
                                'action' => 'editTag'
                            ),
                        ),
                    ),
                    'deletePost' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/delete/post/:id',
                            'constraints' => array(
                                'id' => '[1-9]\d*',
                            ),
                            'defaults' => array(
				'controller' => 'Blog\Controller\delete',
				'action' => 'deletePost'
                            ),
                        ),
                    ),
                    'deleteTag' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/delete/tag/:id',
                            'constraints' => array(
                                'id' => '[1-9]\d*',
                            ),
                            'defaults' => array(
                                'controller' => 'Blog\Controller\delete',
                                'action' => 'deleteTag'
                            ),
                        ),
                    ),
                    'deleteCategory' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/delete/category/:id',
                            'constraints' => array(
                                'id' => '[1-9]\d*',
                            ),
                            'defaults' => array(
                                'controller' => 'Blog\Controller\delete',
                                'action' => 'deleteCategory'
                            ),
                        ),
                    ),
                ),
            ),
            
            
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml'
        ),
        'template_path_stack' => array(
            'Blog' => __DIR__ . '/../view',
        ),
    ),
);
